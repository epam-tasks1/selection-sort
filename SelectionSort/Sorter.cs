﻿using System;

#pragma warning disable SA1611

namespace SelectionSort
{
    public static class Sorter
    {
        public static void SelectionSort(this int[] array)
        {
            if (array is null)
            {
                throw new ArgumentNullException(nameof(array));
            }

            for (int i = 0; i < array.Length; i++)
            {
                int valueIndex = i;
                int minimalValue = array[i];
                for (int j = i; j < array.Length; j++)
                {
                    if (array[j] < minimalValue)
                    {
                        minimalValue = array[j];
                        valueIndex = j;
                    }
                }

                (array[i], array[valueIndex]) = (array[valueIndex], array[i]);
            }
        }

        public static void RecursiveSelectionSort(this int[] array)
        {
            if (array is null)
            {
                throw new ArgumentNullException(nameof(array));
            }

            if (array.Length > 1)
            {
                RecursiveSelectionSortByIndex(array, 0);
            }
        }

        public static int RecursiveFindIndexOfMin(this int[] array, int minValue)
        {
            if (array is null)
            {
                throw new ArgumentNullException(nameof(array));
            }

            if (array.Length <= 1)
            {
                return 0;
            }

            int minIndex = RecursiveFindIndexOfMin(array[1..], minValue) + 1;

            if (array[0] < array[minIndex])
            {
                return 0;
            }
            else
            {
                return minIndex;
            }
        }

        public static void RecursiveSelectionSortByIndex(this int[] array, int index)
        {
            if (array is null)
            {
                throw new ArgumentNullException(nameof(array));
            }

            int indexOfMinValue = RecursiveFindIndexOfMin(array[index..], array[index]) + index;

            (array[index], array[indexOfMinValue]) = (array[indexOfMinValue], array[index]);

            if (index != array.Length - 1)
            {
                index++;
                RecursiveSelectionSortByIndex(array, index);
            }
        }
    }
}
